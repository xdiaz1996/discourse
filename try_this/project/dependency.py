import operator


def get_text_list(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values() if node["address"] is not None]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["word"] is not None:
            tagged_question.append(node["word"])
    return tagged_question


def find_main(graph):
    for node in graph.nodes.values():
        if node['rel'] == 'root':
            return node
    return None


def get_address(graph, address):
    return graph["dep"].get_by_address(address)


def find_node_by_tag(tag, graph):
    for node in graph.nodes.values():
        if node["tag"] == tag:
            return node
    return None
