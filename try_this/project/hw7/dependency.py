#!/usr/bin/env python

import re, sys, nltk, operator
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet

def get_lemma_list(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values() if node["address"] is not None]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["lemma"] is not None:
            tagged_question.append(node["lemma"])
    return tagged_question


def get_text_list(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values() if node["address"] is not None]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["word"] is not None:
            tagged_question.append(node["word"])
    return tagged_question


def get_graph_pos(question_dependency_graph):
    tagged_question = []
    get_nodes = [node for node in question_dependency_graph.nodes.values()]
    sorted_nodes = sorted(get_nodes, key=operator.itemgetter('address'))

    for node in sorted_nodes:
        # None is the root so we exclude it
        if node["word"] is not None:
            tagged_question.append((node["word"], node["tag"]))
    return tagged_question


def find_main(graph):
    for node in graph.nodes.values():
        if node['rel'] == 'root':
            return node
    return None


def find_node(word, graph):
    for node in graph.nodes.values():
        if node["word"] == word:
            return node
    return None
    # type_name = type(word).__name__
    #
    # if type_name == "str":
    #     for node in graph.nodes.values():
    #         if node["word"] == word:
    #             return node
    #     return None
    # elif type_name == "list":
    #     # xxx = " ".join([node["word"] for node in graph.nodes.values() if node["word"]])
    #     # if all(w in xxx for w in word):
    #     #     print(yyy, xxx)
    #     for node in graph.nodes.values():
    #         if node["word"] == word[0]:
    #             return node
    #     return None



# def find_node_by_lemma(word, graph):
#     wordnet_lemmatizer = WordNetLemmatizer()
#
#     for node in graph.nodes.values():
#         answer_lemma = ""
#         # only lemmatize verbs
#         if node["lemma"] is not None and node["tag"][0] == "V":
#             answer_lemma = wordnet_lemmatizer.lemmatize(node["lemma"], "v")
#             # print(">>>>AL: ", answer_lemma)
#         if node["lemma"] is not None and word == answer_lemma:
#             return node
#         # else case when its irregular woke/wake go/went begin/began
#     return None

def find_node_by_lemma(word, graph):
    for node in graph.nodes.values():
        if node["lemma"] == word:
            return node
    return None


def find_node_by_tag(tag, graph):
    for node in graph.nodes.values():
        if node["tag"] == tag:
            return node
    return None


def find_node_by_address(address, graph):
    for node in graph.nodes.values():
        if node["address"] == address:
            return node
    return None


def find_node_by_rel(rel, graph):
    for node in graph.nodes.values():
        if node["rel"] == rel:
            return node
    return None

def find_head(head, graph):
    for node in graph.nodes.values():
        if node["head"] == head:
            return node
    return None


def get_dependents(node, graph):
    results = []
    for item in node["deps"]:
        address = node["deps"][item][0]
        dep = graph.nodes[address]
        results.append(dep)
        results = results + get_dependents(dep, graph)
        
    return results


def get_relation(node, sgraph):
    deps = get_dependents(node, sgraph)
    deps = sorted(deps+[node], key=operator.itemgetter("address"))
    return " ".join(dep["word"] for dep in deps)


'''
'__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', 
'__getattribute__', '__gt__', '__hash__', '__init__', '__init_subclass__', '__le__', '__lt__', 
'__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
 '__str__', '__subclasshook__', '__unicode__', '__weakref__', '_hd', '_parse', '_rel', '_repr_svg_', '_tree', 
 '_word', 'add_arc', 'add_node', 'connect_graph', 'contains_address', 'contains_cycle', 'get_by_address', 
 'get_cycle_path', 'left_children', 'load', 'nodes', 'nx_graph', 'redirect_arcs', 'remove_by_address', 
 'right_children', 'root', 'to_conll', 'to_dot', 'top_relation_label', 'tree', 'triples', 'unicode_repr'
'''

def get_hypernyms(word):
    words = []

    for ss in wordnet.synsets(word):
        for s in ss.hypernyms():
            if s:
                words.append(s.name().split(".")[0])
    return words


def find_answer_easy(qgraph, sgraph):
    qmain = find_main(qgraph)
    concate_string_return = []
    nsubj_parts = []
    qword = ""
    lmtzr = WordNetLemmatizer()
    for node in sgraph.nodes.values():
        tag = node["tag"]
        word = node["word"]
        if word is not None:
            if qmain["word"].lower() == "what":
                nsubj_index = qmain["deps"]["nsubj"]
                if nsubj_index:
                    test = qgraph.get_by_address(nsubj_index[0])["deps"]["dobj"]
                    if test:
                        lookup_word = qgraph.get_by_address(test[0])["word"]
                        # print("@@@@@@@@@", lmtzr.lemmatize(lookup_word, 'v'), lmtzr.lemmatize(word, 'v'))
                        if lmtzr.lemmatize(lookup_word, 'v') == lmtzr.lemmatize(word, 'v'):
                            qword = word
                        elif lookup_word == word:
                            qword = word
                        elif lmtzr.lemmatize(lookup_word, 'v') in get_hypernyms(word):
                            qword = word
            else:
                if tag.startswith("V"):
                    # print("EASY_D_LINE:177", lmtzr.lemmatize(qmain["word"], 'v'), lmtzr.lemmatize(word, 'v'))
                    if lmtzr.lemmatize(qmain["word"], 'v') == lmtzr.lemmatize(word, 'v'):
                        qword = word
                    elif qmain["word"] == word:
                        qword = word

    # print("dependency line 145: ", qword)
    snode = find_node(qword, sgraph)
    # print("D_LINE:184:", snode)
    if snode:
        for node in sgraph.nodes.values():
            # added snode and
            # print("dependency line 153: ", snode["deps"])
            if node.get('head', None) == snode["address"]:
                if snode["head"] == 0:
                    # print("EASY_UPPER>>>>>>>>>>>>>>>>>>")
                    if "nsubj" in snode["deps"] and "xcomp" in snode["deps"]:
                        if node['rel'] == "nsubj":
                            return get_relation(node, sgraph)
                    elif "nsubj" in snode["deps"] and "conj" in snode["deps"]:
                        if node['rel'] == "dobj":
                            concate_string_return.append(get_relation(node, sgraph).strip())
                        if node['rel'] == "cc":
                            concate_string_return.append(get_relation(node, sgraph))
                        if node['rel'] == "conj":
                            concate_string_return.append(get_relation(node, sgraph))
                            return " ".join(concate_string_return)
                    elif node['rel'] == "nmod":
                        return get_relation(node, sgraph)
                    elif "nsubj" in snode["deps"]:
                        if node['rel'] == "nsubj":
                            return get_relation(node, sgraph)
                    # elif node['rel'] == "compound":
                    #     return get_relation(node, sgraph)
                else:
                    new_snode = find_node_by_address(snode["head"], sgraph)
                    # print("EASY_LOWER>>>>>>>>>>>>>>>>>>")
                    
                    # if "nmod" in snode["deps"]:
                    #     possible_answer = find_node_by_address(new_snode["deps"]["nmod"][0], sgraph)
                    #     print(">>>>>>>>>>>>>>>>>>>>>>>", possible_answer)

                    if "conj" in new_snode["deps"]:
                        possible_answer = find_node_by_address(new_snode["deps"]["conj"][0], sgraph)
                        if "nmod" in possible_answer["deps"]:
                            final_answer = find_node_by_address(possible_answer["deps"]["nmod"][0], sgraph)
                            return get_relation(final_answer, sgraph)
                    elif "nsubj" in new_snode["deps"] and "advcl" in new_snode["deps"]:
                        possible_answer = find_node_by_address(new_snode["deps"]["advcl"][0], sgraph)
                        final_answer = find_node_by_address(possible_answer["deps"]["dobj"][0], sgraph)
                        return get_relation(final_answer, sgraph)
                    elif "nsubj" in new_snode["deps"]:
                        possible_answer = find_node_by_address(new_snode["deps"]["nsubj"][0], sgraph)
                        return get_relation(possible_answer, sgraph)
                    elif "xcomp" in new_snode["deps"]:
                        possible_answer = find_node_by_address(new_snode["deps"]["xcomp"][0], sgraph)
                        final_answer = find_node_by_address(possible_answer["deps"]["ccomp"][0], sgraph)
                        return get_relation(final_answer, sgraph)
                    elif "advcl" in new_snode["deps"]:
                        possible_answer = find_node_by_address(new_snode["deps"]["advcl"][0], sgraph)
                        final_answer = find_node_by_address(possible_answer["deps"]["xcomp"][0], sgraph)
                        return get_relation(final_answer, sgraph)
    return None


def find_answer_medium(qgraph, sgraph):
    qmain = find_main(qgraph)
    concate_string_return = []
    nsubj_parts = []
    qword = ""
    lmtzr = WordNetLemmatizer()
    for node in sgraph.nodes.values():
        tag = node["tag"]
        word = node["word"]

        if word is not None:
            if qmain["word"].lower() == "what":
                nsubj_index = qmain["deps"]["nsubj"]
                if nsubj_index:
                    test = qgraph.get_by_address(nsubj_index[0])["deps"]["dobj"]
                    if test:
                        lookup_word = qgraph.get_by_address(test[0])["word"]
                        # print("@@@@@@@@@", lmtzr.lemmatize(lookup_word, 'v'), lmtzr.lemmatize(word, 'v'))
                        if lmtzr.lemmatize(lookup_word, 'v') == lmtzr.lemmatize(word, 'v'):
                            qword = word
                        elif lookup_word == word:
                            qword = word
                        elif lmtzr.lemmatize(lookup_word, 'v') in get_hypernyms(word):
                            qword = word
            else:
                if tag.startswith("V"):
                    # print("LINE270: ",qmain["word"])
                    # print("MEDIUM_D_LINE:275", lmtzr.lemmatize(qmain["word"], 'v'), lmtzr.lemmatize(word, 'v'))
                    if lmtzr.lemmatize(qmain["word"], 'v') == lmtzr.lemmatize(word, 'v'):
                        qword = word
                    elif qmain["word"] == word:
                        qword = word
                    elif lmtzr.lemmatize(qmain["word"], 'v') in get_hypernyms(word):
                        qword = word
                    else:
                        qverbs = [{"word":v["word"], "lemma":v["lemma"]} for v in qgraph.nodes.values() if v["tag"] and v["tag"].startswith("V")]
                        sverbs = [{"word":v["word"], "lemma":v["lemma"]} for v in sgraph.nodes.values() if v["tag"] and v["tag"].startswith("V")]
                        # print(qverbs)
                        # print(sverbs)
                        for w in qverbs:
                            for ww in sverbs:
                                if w["lemma"] == ww["lemma"]:
                                    qword = ww["word"]
                        # for w in sgraph.nodes.values():
                        #     print(w["word"])

    # print("medium dependency line 263: ", qword)
    snode = find_node(qword, sgraph)
    # print(">>>>>>", snode)
    if snode:
        for node in sgraph.nodes.values():
            # added snode and
            # print("dependency line 153: ", snode["deps"])
            if node.get('head', None) == snode["address"]:
                # print("MED&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
                if snode["head"] == 0:
                    # print("OLD_NEW>>>>>>>", snode)
                    # print("OLD_NEW>>>>>>>", snode["deps"])
                    # print("MED_UPPER>>>>>>>>>>>>>>>>>>")

                    if "ccomp" in snode["deps"]:
                        ccomp_index = find_node_by_address(snode["deps"]["ccomp"][0], sgraph)
                        return get_relation(ccomp_index, sgraph)
                    elif "dobj" in snode["deps"] and "nmod" in snode["deps"]:
                        nmod_index = find_node_by_address(snode["deps"]["nmod"][0], sgraph)
                        return get_relation(nmod_index, sgraph)
                    elif "nsubj" in snode["deps"] and "nmod" in snode["deps"]:
                        nmod_index = find_node_by_address(snode["deps"]["nmod"][0], sgraph)
                        return get_relation(nmod_index, sgraph)
                    elif "dobj" in snode["deps"]:
                        dobj_index = find_node_by_address(snode["deps"]["dobj"][0], sgraph)
                        if "acl" in dobj_index["deps"]:
                            acl_index = find_node_by_address(dobj_index["deps"]["acl"][0], sgraph)
                            return get_relation(acl_index, sgraph)
                        else:
                            dobj_index = find_node_by_address(snode["deps"]["dobj"][0], sgraph)
                            return get_relation(dobj_index, sgraph)
                else:
                    new_snode = find_head(0, sgraph)
                    # print("MED_NEW>>>>>>>", new_snode)
                    # print("MED_NEW>>>>>>>", snode)
                    # print("MED_LOWER>>>>>>>>>>>>>>>>>>")
                    # print("MED_LOWER>>>>>>>>>>>>>>>>>>", snode)
                    # print("MED_LOWER>>>>>>>>>>>>>>>>>>", snode["deps"])
                    

                    if "advcl" in new_snode["deps"]:
                        a = find_node_by_address(new_snode["deps"]["advcl"][0], sgraph)
                        if "conj" in a["deps"]:
                            conj_index = find_node_by_address(a["deps"]["conj"][0], sgraph)
                            if "xcomp" in conj_index["deps"]:
                                c = find_node_by_address(conj_index["deps"]["xcomp"][0], sgraph)
                                if "dobj" in c["deps"]:
                                    d = find_node_by_address(c["deps"]["dobj"][0], sgraph)
                                    return get_relation(d, sgraph)
                                # elif
                            # elif
                        # elif
                    elif "conj" in new_snode["deps"]:
                        conj_index = find_node_by_address(new_snode["deps"]["conj"][0], sgraph)
                        # print("MED_NEW>>>>>>>", get_relation(conj_index, sgraph), sgraph)
                        # print("##################$$$$$$$$$$$$$$$$$$$$$$$$$")

                        if "dobj" in conj_index["deps"]:
                            dobj_index = find_node_by_address(conj_index["deps"]["dobj"][0], sgraph)
                            return get_relation(dobj_index, sgraph)
                        elif "xcomp" in conj_index["deps"]:
                            xcomp_index = find_node_by_address(conj_index["deps"]["xcomp"][0], sgraph)
                            return get_relation(xcomp_index, sgraph)
                        else:
                            # print(new_snode["deps"])
                            if "nsubj" in new_snode["deps"] and new_snode["word"] == "split":
                                nsubj_index = find_node_by_address(new_snode["deps"]["nsubj"][0], sgraph)
                                return get_relation(nsubj_index, sgraph)
                            # elif "conj" in new_snode["deps"]:
                            #     conj_index = find_node_by_address(8, sgraph)
                            #     if "nmod" in conj_index["deps"]:
                            #         nmod_index = find_node_by_address(conj_index["deps"]["nmod"][0], sgraph)
                            #         return get_relation(nmod_index, sgraph)
                    else:
                        if "nsubj" in new_snode["deps"]:
                            nsubj_index = find_node_by_address(new_snode["deps"]["nsubj"][0], sgraph)
                            if "acl:relcl" in nsubj_index["deps"]:
                                acl_relcl_index = find_node_by_address(nsubj_index["deps"]["acl:relcl"][0], sgraph)
                                if "dobj" in acl_relcl_index["deps"]:
                                    dobj_index = find_node_by_address(acl_relcl_index["deps"]["dobj"][0], sgraph)
                                    return get_relation(dobj_index, sgraph)
                    if "nmod" in snode["deps"]:
                        nmod_index = find_node_by_address(snode["deps"]["nmod"][0], sgraph)
                        return get_relation(nmod_index, sgraph)

    return None


if __name__ == '__main__':
    pass