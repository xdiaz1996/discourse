from base import QABase
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from score_answer import main as score_answers
from questions import (get_question_text, get_question_category, get_question_verbs,
                       get_question_nouns, get_question_type)
from dependency import get_text_list
from nltk.stem import *


def find_similarities(question_list, sentence_list):
    a = set(question_list)
    b = set(sentence_list)
    result = (a & b)

    if len(result) > 0:
        # print(result)
        return True
    else:
        return False


def get_hypernyms(word):
    words = []

    for ss in wordnet.synsets(word):
        for s in ss.hypernyms():
            if s:
                words.append(s.name().split(".")[0])
    return words


def get_lemmas(word):
    words = []

    for ss in wordnet.synsets(word):
        for s in ss.hypernyms():
            for x in s.lemmas():
                if x.name() not in words:
                    words.append(x.name())
    return words


def lemmatizer(verb, type):
    """
    Finds the root word for a given word.
    :param verb: string to find the root of
    :param type: word type (noun, verb, etc)
    :return: string
    """
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmatize = wordnet_lemmatizer.lemmatize(verb, type)

    return lemmatize


def sentence_selection(nouns, verbs, story_dependency_graph, story_text):
    print(verbs)
    print(nouns)
    index_found = 0
    index_found_2 = 0
    all_sentences = list(enumerate(story_dependency_graph))
    for index, sentence_graph in all_sentences:
        # print(" ".join(get_text_list(sentence_graph)))
        v_found_1 = False
        v_found_2 = False
        v_found_3 = False
        v_found_4 = False
        n_found_1 = False
        n_found_2 = False
        tmp_sent = sentence_graph
        for node in sentence_graph.nodes.values():
            tag = node["tag"]
            word = node["word"]
            lemma = node["lemma"]
            noun_keys = nouns.keys()
            verb_keys = verbs.keys()


            if word and "base" in verb_keys and "noun_subject" in noun_keys:
                verb = verbs["base"]
                noun = nouns["noun_subject"]
                if verb and noun:
                    # print(word, get_hypernyms(verb), get_hypernyms(word))

                    if tag.startswith("V"):
                        if lemmatizer(verb, "v") == lemmatizer(word, "v"):
                            v_found_1 = True
                    else:
                        if noun == word:
                            n_found_1 = True

            if word and "past_tense" in verb_keys and "noun_subject" in noun_keys:
                verb = verbs["past_tense"]
                noun = nouns["noun_subject"]
                if verb and noun is None:
                    # print(">>>>>>")
                    if tag.startswith("V"):
                        lemm = lemmatizer(verb, "v")
                        if lemm == lemma:
                            v_found_2 = True
                            index_found = index
            if word and "past_participle" in verb_keys and "noun_subject" in noun_keys:
                verb = verbs["past_participle"]
                noun = nouns["noun_subject"]
                # print(verb, noun)
                if verb and noun is None:
                    if tag.startswith("V"):
                        if lemmatizer(verb, "v") == lemmatizer(word, "v"):
                            v_found_3 = True
                        # else ing
            if word and "base" in verb_keys:
                verb = verbs["base"]
                if verb:
                    if tag.startswith("V"):
                        if lemmatizer(verb, "v") == lemmatizer(word, "v"):
                            v_found_4 = True
                        elif verb[:5] == word[:5]:
                            v_found_4 = True

                            




            if n_found_1 and v_found_1:
                # print(" ".join(get_text_list(sentence_graph)))
                return sentence_graph
            elif v_found_2:
                # print(" ".join(get_text_list(all_sentences[index_found-1][1])))
                return all_sentences[index_found-1][1]
            elif v_found_3:
                # print(" ".join(get_text_list(sentence_graph)))
                return sentence_graph
            elif v_found_4:
                # print(" ".join(get_text_list(sentence_graph)))
                return sentence_graph


def get_answer(question, story):
    question_text = get_question_text(question)
    question_category = get_question_category(question)
    
    # print("[*] Category:", question_category)
    # print("[*] Question Verbs:", get_question_verbs(question))
    # print("[*] Question Nouns:", get_question_nouns(question))
    # print("\n")
    # ############################################

    if question_category == "what":
        print("[*] Question:", question_text)
        question_type = get_question_type(question).lower()
        verbs = get_question_verbs(question)
        nouns = get_question_nouns(question)

        if question_type == "story":
            story_text = story["text"]
            story_dependency_graph = story["story_dep"]

            sentence_graph = sentence_selection(nouns, verbs, story_dependency_graph, story_text)
            if sentence_graph:
                print(" ".join(get_text_list(sentence_graph)))
        elif question_type == "sch":
            sch_text = story["sch"]
            sch_dependency_graph = story["sch_dep"]

            sentence_selection(nouns, verbs, sch_dependency_graph, sch_text)
        print("\n\n")
    # ############################################


class QAEngine(QABase):
    @staticmethod
    def answer_question(question, story):
        answer = get_answer(question, story)
        return answer


def run_qa(save):
    QA = QAEngine()
    
    QA.run()
    if save:
        QA.save_answers()


def main(score=False, save=False):
    run_qa(save)
    if score:
        score_answers()

if __name__ == "__main__":
    main()
